import java.util.ArrayList;

/**
 * This is the container class for holding Users. 
 * @author tashaik
 *
 */
public class UserCollection {
	
	private ArrayList<User> userList;
	
	/**
	 * This is the default constructor for creating a User Collection
	 */
	public UserCollection() {
		userList = new ArrayList<User>();
	}

}
