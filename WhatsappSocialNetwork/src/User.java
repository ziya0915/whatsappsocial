import java.util.Random;

/**
 * This is the User class implemetnation. This user is the 
 * user of the social network.
 * @author Puran
 *
 */
public class User {
	
	private String id;
	private String name;
	@SuppressWarnings("unused")
	private String password;
	private String status;
	
	/**
	 * First constructor. This creates a new user account using
	 * username and password
	 * @param name			username of User
	 * @param password		password of User
	 */
	public User(String name, String password) {
		this.id = this.givenUsingJava8_whenGeneratingRandomAlphanumericString_thenCorrect();
		this.name = name;
		this.password = password;
		this.setStatus("");
	}

	/**
	 * Second constructor. This creates a new user account using
	 * username and password and status.
	 * @param name			username of User
	 * @param password		password of User
	 * @param status		status of  User
	 */
	public User(String name, String password, String status) {
		this.id = this.givenUsingJava8_whenGeneratingRandomAlphanumericString_thenCorrect();
		this.name = name;
		this.password = password;
		this.setStatus(status);
	}
	
	@Override
	public String toString() {
		String returnString = "\nUser details below \n";
		returnString += " id:             " + this.id + "\n";
		returnString += " name:           " + this.name + "\n";
		returnString += " status:         " + this.status + "\n";
	
		return returnString;
	}
	
	/**
	 * This is a random string generator stolen from below site.
	 * https://www.baeldung.com/java-random-string
	 * @return
	 */
	private String givenUsingJava8_whenGeneratingRandomAlphanumericString_thenCorrect() {
	    int leftLimit = 48; // numeral '0'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = 10;
	    Random random = new Random();

	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();

	    return generatedString;
	}

	public String getName() {
		return name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
